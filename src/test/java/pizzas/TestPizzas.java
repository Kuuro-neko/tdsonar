package pizzas;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestPizzas {

	private BasePizzas base=new BasePizzas();
	
	@BeforeEach
	public void init() {
		base.addPizzaToMenu(base.createSurpriseWhitePizza());
	}
	
	@Test
	void testAjoutPizza() {
		Pizza p=new Pizza("fromages", 10);
		p.ajoutIngredient(new Ingredient("Mozzarelle", true));
		p.ajoutIngredient(new Ingredient("Talegio", true));
		base.addPizzaToMenu(p);
		assertEquals(p, base.getPizzaFromMenu("fromages"));
	}
	
	@Test
	 void testAjoutIng1() {
		Pizza p=base.getPizzaFromMenu("Surprise blanche");
		System.out.println(p.formattedIngredients());
		var oldSize=p.ingredients().length;
		p.ajoutIngredient(new Ingredient("brocolis", true));
		assertEquals(oldSize+1, p.ingredients().length);
	}

	@Test
	void testVeganize() {
		Pizza p = new Pizza("vegan", 1F);
		p.ajoutIngredient(new Ingredient("champignon", true));
		p.ajoutIngredient(new Ingredient("jambon", false));
		p.veganize();
		Pizza p2 = new Pizza("vegan", 1F);
		p2.ajoutIngredient(new Ingredient("champignon", true));
		assertEquals(p.formattedIngredients(), p2.formattedIngredients());
	}

	@Test
	void testGetPrix() {
		Pizza p = new Pizza("a", 1F);
		assertEquals(1F, p.getPrix());
	}

	@Test
	void testEstVegetarienneTrue() {
		Pizza p = new Pizza("a", 1F);
		p.ajoutIngredient(new Ingredient("champignon", true));
		assertTrue(p.estVegetarienne());
	}

	@Test
	void testEstVegetarienneFalse() {
		Pizza p = new Pizza("a", 1F);
		p.ajoutIngredient(new Ingredient("jambon", false));
		assertFalse(p.estVegetarienne());
	}

	@Test
	void testPizzaEstBlancheFalseSansBase() {
		Pizza p = new Pizza("a", 1F);
		assertFalse(p.estBlanche());
	}
	@Test
	void testBaseBlancheTrue() {
		Pizza p = new Pizza("a", 1F);
		p.ajoutIngredient(new Ingredient("crème fraîche", true));
		assertTrue(p.estBlanche());
	}

	@Test
	void testBaseBlancheFalse() {
		Pizza p = new Pizza("a", 1F);
		p.ajoutIngredient(new Ingredient("sauce tomate", true));
		assertFalse(p.estBlanche());
	}

	@Test
	void testEstRougeTrue() {
		Pizza p = new Pizza("a", 1F);
		p.ajoutIngredient(new Ingredient("sauce tomate", true));
		assertTrue(p.estRouge());
	}

	@Test
	void testEstRougeFalse() {
		Pizza p = new Pizza("a", 1F);
		p.ajoutIngredient(new Ingredient("crème fraîche", true));
		assertFalse(p.estRouge());
	}

	@Test
	void testIngExistTrue() {
		assertTrue(base.exists("chorizo"));
	}

	@Test
	void testIngExistFalse() {
		assertFalse(base.exists("adfsgfgh"));
	}

	@Test
	void testPizzaWithMissingIngredients() {
		Pizza p = new Pizza("a", 1f);
		p.ajoutIngredient(new Ingredient("carte graphique", true));
		base.addPizzaToMenu(p);
		assertTrue(base.pizzasWithMissingIngredient().contains(p));
	}

	@Test
	void testPizzaWithoutMissingIngredients() {
		Pizza p = new Pizza("a", 1f);
		p.ajoutIngredient(new Ingredient("chorizo", false));
		base.addPizzaToMenu(p);
		assertFalse(base.pizzasWithMissingIngredient().contains(p));
	}

	@Test
	void testPizzaSurpriseMissingIngredients() {
		Pizza p = base.createSurpriseWhitePizza();
		base.addPizzaToMenu(p);
		assertFalse(base.pizzasWithMissingIngredient().contains(p));
	}

	@Test
	void testIngredientEquals() {
		Ingredient i1 = new Ingredient("brocoli", true);
		Ingredient i2 = new Ingredient("brocoli", true);
		assertEquals(i1, i2);
		assertEquals(i1.hashCode(), i2.hashCode());
	}

	@Test
	void testPizzaEquals() {
		Pizza p1 = new Pizza("a", 1F);
		p1.ajoutIngredient(new Ingredient("crème fraîche", true));
		Pizza p2 = new Pizza("a", 1F);
		p2.ajoutIngredient(new Ingredient("crème fraîche", true));
		assertEquals(p1, p2);
		assertEquals(p1.hashCode(), p2.hashCode());
	}
}
